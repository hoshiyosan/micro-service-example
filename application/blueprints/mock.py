from flask import Blueprint

bp = Blueprint("mock", __name__)

@bp.route('/')
def home():
    return "Hello everyone"

@bp.route('/api/resource')
def mock_endpoint():
    return {"resource": "a value"}
