from flask import Flask
from flask_cors import CORS

from .blueprints import mock_blueprint
from . import settings


class Application(Flask):
    def __init__(self):
        Flask.__init__(self, __name__)
        self.config.from_object(settings)
        self.register_extensions()
        self.register_blueprints()


    def register_extensions(self):
        cors.init_app(self)


    def register_blueprints(self):
        self.register_blueprint(mock_blueprint)


app = Application()
