# This file is a template, and might need editing before it works on your project.
FROM python:3.8-alpine

WORKDIR /app

COPY requirements /tmp/requirements
RUN pip install --no-cache-dir -r /tmp/requirements

COPY . /app

EXPOSE 80
CMD ["gunicorn", "-b", "0.0.0.0:80", "application.application:app"]
